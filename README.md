Tutorial for Harel for the git cli
=======
Please follow the next steps:

1. Create a folder named: "git-demo" in your computer. 
2. Enter this folder using ```cd git-demo```
3. Initialize an empty git repository using ```git init```
4. Use ```git status``` to understand: which branch are you on ? way ?
5. Create 3 new text files (add some content to each one of the files)
6. Use ```git status``` to understand the status of the files in the git repository
7. Use ``` git add .``` to add the files to git (staged)
8. Use ```git status``` to understand the status of the files now, why ?
9. Use ```git commit -am "initial commit"``` to commit the files
10. Use ```git status``` to understand the status of the files now, why ?
11. Use ```git log``` to see the log of git
12. Use ```git log --pretty=oneline```, what is the difference with the previos command
13. Create a new directory (in the git-demo directory) named "bin"
14. Create a new text file in the "bin" directory
15. Use ```git status``` to understand the status of the files now, why ?
16. Create a file in the directory "git-demo" named ".gitignore" (this file startes with "." and has no file extension)
17. In the ".gitignore" file add the following text (as line): ```bin/```
18. Use ```git status``` to understand the status of the files now, why ? 
19. Use ```git add .gitignore``` to add this file
20. Use ```git commit -am "added ignore list"```
21. Use ```git status``` to understand the status of the files now, why ? 
22. Use ```git log``` to see the log of git
23. Use ```git branch``` command to see list of branches
24. Use ```git checkout -b {name}/{brnach_name}``` to checkout to a new local branch
25. Use ```git status``` to understand on which branch are you on
26. Use ```git branch``` command to see list of branches
27. Create a new file and edit it
28. Add this file to git and commit
29. Return to the master branch using ```git checkout master```
30. Look for the new file you just created (in section 27), is it exist ? why ?
31. Use ```git merge {name}/{brnach_name}``` to merge the {name}/{brnach_name} branch to master
32. Use ```git status``` to understand your git status
33. Look for the new file you just created (in section 27), is it exists now ? why ?
34. Use ```git branch -D {name}/{brnach_name}``` to delete the previous branch you created
35. Use ```git branch``` command to see list of branches, why ?
36. Use ```git status``` to understand your git status
37. Use ```git reflog``` to see the references log of git
38. Use ```git log``` and identify the first commit you did, copy its sha-1 key (commit ID)
39. Use ```git checkout {commit ID}``` to checkout to this commit
39. On which branch are you on now ? why ? 
40. Use ```git tag```, which tags do you see ? why?
41. Use ```git tag -a v1.0 -m "first version"``` to create a tag
42. Now use ```git tag```, which tags do you see ?
43. Use ```git checkout master``` to return to master
44. Use ```git reflog``` to see the references log of git
45. Use ```git status``` to understand your git status
46. Use ```git checkout v1.0``` to return to version 1.0
47. Use ```git status``` to understand your git status
48. Use ```git checkout master``` to return to master


Gtlab as remote git
=======
1.  Use ```git remote -v``` to see list of remote gits
2.  How many remotes do you have ?
3.  Create a new empty repository (blank project) in gitlab (without readme file)
4.  Copy the https url of the newly created repository (from the "clone" link)
5.  Use ```git remote add origin {repository_url}```
6.  Use ```git remote -v``` to see list of remote gits
7.  Use ```git push -u origin master``` to push the master branch to the origin remote
8.  Use ```git status``` to understand your git status, what is different now ?
9.  Go to gitlab to the new project created and refresh, what do you see now ? 
10.  How many branches do you see?
11.  In gitlab, create new branch named "{name}/{brnach_name}" (from the master branch)
12.  In gitlab add a new file to this branch and commit
13.  In gitlab, create a merge request from the new branch you created to the master bracnh
14.  Submit the merge request but DONT (!!!!) merge yet
15.  Now, retrun to your local git repository]
16.  Use ```git branch``` command to see list of branches, do you see the new branch you created ? why?
17.  Return to gitlab, to the merge request and click "merge" (make sure to uncheck the "Delete source branch")
18.  After the merge, return to the master branch, when was the last commit ? why
19.  In gitlab, go the commits list (Repository -> commits), what is the latest commit id (SHA)
19.  Return to your local git repository, on master branch, use ```git status```, what is the status ?
20.  Use ```git log``` what is the last commit id ? Is it the same as the remote (gitlab) last commit? why ?
21.  Use ```git fetch```, what do you see now ?
22.  Use ```git branch```, how many branches do you see now ? why ? 
23.  Use ```git status``` to understand your git status, what is different now ?
24.  Use ```git pull```
25.  Use ```git status``` to understand your git status, what do you see now ? 
26.  Use ```git checkout {name}/{brnach_name}``` to the same branch you created in gitlab (in section 11), what just happen?



